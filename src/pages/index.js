import {LayoutOne} from "../components/Layout";
import {ProductTab} from "../components/ProductTab";
import heroSliderData from "../data/hero-sliders/hero-slider-two.json";
import products from "../data/products.json";
import {getProductForGender} from "../lib/product";
import HeroSliderTwo from "../components/HeroSlider/HeroSliderTwo";


const Home = ({menProducts,womenProducts}) => {
  return (
    <LayoutOne aboutOverlay={false}>
      {/* hero slider */}
      {/*<HeroSliderOne sliderData={heroSliderData} />*/}
      <HeroSliderTwo sliderData={heroSliderData}/>
      {/*<HeroSliderThree sliderData={heroSliderData}/>*/}
      {/*<HeroSliderFour sliderData={heroSliderData}/>*/}
      {/*<HeroSliderFive sliderData={heroSliderData}/>*/}
      {/*<HeroSliderSix sliderData={heroSliderData}/>*/}
      {/*<HeroSliderSeven sliderData={heroSliderData}/>*/}
      {/* product tab */}
      <ProductTab
          products={products}
          menProducts={getProductForGender(products,"men")}
          womenProducts={getProductForGender(products,"women")}
      />

      {/* image cta */}
      {/*<ImageCta*/}
      {/*  image={imageCtaData.image}*/}
      {/*  tags={imageCtaData.tags}*/}
      {/*  title={imageCtaData.title}*/}
      {/*  url={imageCtaData.url}*/}
      {/*/>*/}
    </LayoutOne>
  );
};

// const mapStateToProps = (state) => {
//   const products = state.productData;
//   return    {
//     // newProducts: getProducts(products, "decor", "new", 9),
//     // popularProducts: getProducts(products, "decor", "popular", 9),
//     // saleProducts: getProducts(products, "decor", "sale", 9)
//       menProducts: getProductForGender(products,"men"),
//       womenProducts: getProductForGender(products,"women")
//   };
// };

// export default connect(mapStateToProps)(Home);
export default Home;
