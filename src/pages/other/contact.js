import {Col, Container, Row} from "react-bootstrap";
import {LayoutTwo} from "../../components/Layout";
import StroeDetails from "./storeDetails";
import {SectionTitleTwo} from "../../components/SectionTitle";
import React from "react";

const Contact = () => {
  return (
    <LayoutTwo>
      {/* breadcrumb */}
      {/*<BreadcrumbOne*/}
      {/*  pageTitle="Contact"*/}
      {/*  backgroundImage="/assets/images/backgrounds/breadcrumb-bg-1.png"*/}
      {/*>*/}
      {/*  <ul className="breadcrumb__list">*/}
      {/*    <li>*/}
      {/*      <Link href="/" as={process.env.PUBLIC_URL + "/"}>*/}
      {/*        <a>Home</a>*/}
      {/*      </Link>*/}
      {/*    </li>*/}

      {/*    <li>Contact</li>*/}
      {/*  </ul>*/}
      {/*</BreadcrumbOne>*/}
      <div className="contact-page-content-wrapper space-mt--r130 space-mb--r130">
        <div className="contact-page-top-info space-mb--r100">
          <Container>
            <Row id={"about-us"}>
              <Col className="text-center mb-5" lg={12}>
                <h1 className={"mb-4"}>About Us</h1>
                <h4 style={{lineHeight:"1.75rem"}} className="lead text-justify">Innovative and progressive, AZLAN is reinventing a wholly modern approach to fashion. Under the new vision of creative director KHAN, the House has redefined T-shirt style for the 21st century, further reinforcing its position as one of the desirable street wear fashion brand. Sporty, contemporary-AZLAN products represent the pinnacle of craftsmanship and are unsurpassed for their quality and attention to detail</h4>
              </Col>
            </Row>

            <Row>
              <Col lg={12}>
                <SectionTitleTwo
                  title="Contact detail"
                  subtitle="COME HAVE A LOOK"
                />
              </Col>
            </Row>
            <StroeDetails
                number={"01"}
                name={"AZLAN"}
                address="Level-3, Sunvalley Shopping Center1 Sunvalley Mall Road, Concord, CA 94520, United States"
                mobile="+1 407-851-6255"
                hourOfOperations={"Sunday - Saturday: 10:00 AM-7:00 PM"}
            />
            <StroeDetails
                number={"02"}
                name={"AZLAN"}
                address="St. Augustine's Indoor Mall 500 Outlet Mall Blvd, St. Augustine, FL 32084, United States"
                mobile="+1 904-826-1311"
                hourOfOperations={"Sunday - Saturday: 10:00 AM-7:00 PM"}
            />

            <StroeDetails
                number={"03"}
                name={""}
                address="Shop # F-09, 1st Floor, Baclaran Bagong Milenyo Plaza,Baclaran,Parañaque, Metro Manila, Philippines"
                mobile="+639568539256"
                hourOfOperations={"Sunday - Saturday: 7:00 AM-5:00 PM"}
            />

            <StroeDetails
                number={"04"}
                name={""}
                address="Shop # 2L-27,2nd Floor, 999 Shopping Mall (Building 2) 1018 Soler Street, Binondo, Manila, 1006 Metro Manila,"
                mobile="+63 909 559 8065"
                hourOfOperations={"Sunday - Saturday: 9:00 AM-6:00 PM"}
            />

          </Container>
        </div>
        {/*<div className="contact-page-map space-mb--r100">*/}
        {/*  <Container>*/}
        {/*    <Row>*/}
        {/*      <Col lg={12}>*/}
        {/*        <div className="google-map">*/}
        {/*          <iframe*/}
        {/*            title="map"*/}
        {/*            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6777.000026107364!2d-74.08304414937152!3d40.83212940017352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2f866a80dcc27%3A0x3e3160910d4d5045!2sHoliday%20Inn%20Express%20%26%20Suites%20Meadowlands%20Area!5e0!3m2!1sen!2sbd!4v1581852597883!5m2!1sen!2sbd"*/}
        {/*            allowFullScreen*/}
        {/*          />*/}
        {/*        </div>*/}
        {/*      </Col>*/}
        {/*    </Row>*/}
        {/*  </Container>*/}
        {/*</div>*/}
        {/*<div className="contact-page-form">*/}
        {/*  <Container>*/}
        {/*    <Row>*/}
        {/*      <Col lg={12}>*/}
        {/*        <SectionTitleOne title="Get in touch" />*/}
        {/*      </Col>*/}
        {/*    </Row>*/}
        {/*    <Row>*/}
        {/*      <Col lg={8} className="ml-auto mr-auto">*/}
        {/*        <div className="lezada-form contact-form">*/}
        {/*          <form>*/}
        {/*            <Row>*/}
        {/*              <Col md={6} className="space-mb--40">*/}
        {/*                <input*/}
        {/*                  type="text"*/}
        {/*                  placeholder="First Name *"*/}
        {/*                  name="customerName"*/}
        {/*                  id="customerName"*/}
        {/*                  required*/}
        {/*                />*/}
        {/*              </Col>*/}
        {/*              <Col md={6} className="space-mb--40">*/}
        {/*                <input*/}
        {/*                  type="email"*/}
        {/*                  placeholder="Email *"*/}
        {/*                  name="customerEmail"*/}
        {/*                  id="customerEmail"*/}
        {/*                  required*/}
        {/*                />*/}
        {/*              </Col>*/}
        {/*              <Col md={12} className="space-mb--40">*/}
        {/*                <input*/}
        {/*                  type="text"*/}
        {/*                  placeholder="Subject"*/}
        {/*                  name="contactSubject"*/}
        {/*                  id="contactSubject"*/}
        {/*                />*/}
        {/*              </Col>*/}
        {/*              <Col md={12} className="space-mb--40">*/}
        {/*                <textarea*/}
        {/*                  cols={30}*/}
        {/*                  rows={10}*/}
        {/*                  placeholder="Message"*/}
        {/*                  name="contactMessage"*/}
        {/*                  id="contactMessage"*/}
        {/*                  defaultValue={""}*/}
        {/*                />*/}
        {/*              </Col>*/}
        {/*              <Col md={12} className="text-center">*/}
        {/*                <button*/}
        {/*                  type="submit"*/}
        {/*                  value="submit"*/}
        {/*                  id="submit"*/}
        {/*                  className="lezada-button lezada-button--medium"*/}
        {/*                >*/}
        {/*                  submit*/}
        {/*                </button>*/}
        {/*              </Col>*/}
        {/*            </Row>*/}
        {/*          </form>*/}
        {/*        </div>*/}
        {/*      </Col>*/}
        {/*    </Row>*/}
        {/*  </Container>*/}
        {/*</div>*/}
      </div>
    </LayoutTwo>
  );
};

export default Contact;
