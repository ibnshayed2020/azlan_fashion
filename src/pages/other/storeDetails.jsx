import React from 'react';
import {Col, Row} from "react-bootstrap";
import {IoIosCall, IoIosClock, IoIosPin} from "react-icons/io/index";

const storeDetails = (props) => {
    return (
        <div className="mt-4">
            <h3>Store-{props.number}: {props.name}</h3>
            <Row className="space-mb-mobile-only--m50 mt-4">
                <Col md={4} className="space-mb-mobile-only--50">
                    <div className="icon-box">
                        <div className="icon-box__icon">
                            <IoIosPin />
                        </div>
                        <div className="icon-box__content">
                            <h3 className="title">ADDRESS</h3>
                            <p className="content">
                                {props.address}
                            </p>
                        </div>
                    </div>
                </Col>
                <Col md={4} className="space-mb-mobile-only--50">
                    <div className="icon-box">
                        <div className="icon-box__icon">
                            <IoIosCall />
                        </div>
                        <div className="icon-box__content">
                            <h3 className="title">CONTACT</h3>
                            <p className="content">
                                Mobile: {props.mobile}{" "}
                                {/*<span>Hotline: 1800 – 1102</span>*/}
                            </p>
                        </div>
                    </div>
                    {/*<div className="icon-box">*/}
                    {/*  <div className="icon-box__icon">*/}
                    {/*    <IoIosMail />*/}
                    {/*  </div>*/}
                    {/*  <div className="icon-box__content">*/}
                    {/*    <p className="content"> Mail: contact@lezadastore.com </p>*/}
                    {/*  </div>*/}
                    {/*</div>*/}
                </Col>
                <Col md={4} className="space-mb-mobile-only--50">
                    <div className="icon-box">
                        <div className="icon-box__icon">
                            <IoIosClock />
                        </div>
                        <div className="icon-box__content">
                            <h3 className="title">HOUR OF OPERATION</h3>
                            <p className="content">
                                {props.hourOfOperations}
                            </p>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    );
};

export default storeDetails;