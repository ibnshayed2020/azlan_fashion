import {LayoutThree} from "../../components/Layout";
import {CreativeContent} from "../../components/HomeContent";
import React from "react";

const Creative = () => {
  return (
    <LayoutThree>
      {/* creative content */}
      <CreativeContent />
    </LayoutThree>
  );
};

export default Creative;
