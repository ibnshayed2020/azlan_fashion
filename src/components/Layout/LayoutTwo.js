import { HeaderOne } from "../Header";
import { FooterTwo } from "../Footer";
import React from "react";

const LayoutTwo = ({ children, aboutOverlay }) => {
  return (
    <div>
      <HeaderOne aboutOverlay={aboutOverlay} />
      {children}
      <FooterTwo />
    </div>
  );
};

export default LayoutTwo;
