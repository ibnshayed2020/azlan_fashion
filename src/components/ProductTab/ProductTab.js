import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import { Container, Row } from "react-bootstrap";
import { ProductGridWrapper } from "../ProductThumb";

const ProductTab = ({ products,menProducts,womenProducts }) => {
  return (
    <div className=" mt-5 product-tab space-mb--r100">
      <Container>
        <Tab.Container defaultActiveKey="men">
          <Nav
            variant="pills"
            className="product-tab__navigation text-center justify-content-center space-mb--r60"
          >
            <Nav.Item>
              <Nav.Link eventKey="men">Menswear</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="women">Womenswear</Nav.Link>
            </Nav.Item>
            {/*<Nav.Item>*/}
            {/*  <Nav.Link eventKey="sale">Sale</Nav.Link>*/}
            {/*</Nav.Item>*/}
          </Nav>
          <Tab.Content>
            <Tab.Pane eventKey="men">
              <Row className="space-mb--rm50">
                <ProductGridWrapper
                  products={menProducts}
                  bottomSpace="space-mb--r50"
                />
              </Row>
            </Tab.Pane>
            <Tab.Pane eventKey="women">
              <Row className="space-mb--rm50">
                <ProductGridWrapper
                  products={womenProducts}
                  bottomSpace="space-mb--r50"
                />
              </Row>
            </Tab.Pane>
            {/*<Tab.Pane eventKey="sale">*/}
            {/*  <Row className="space-mb--rm50">*/}
            {/*    <ProductGridWrapper*/}
            {/*      products={saleProducts}*/}
            {/*      bottomSpace="space-mb--r50"*/}
            {/*    />*/}
            {/*  </Row>*/}
            {/*</Tab.Pane>*/}
          </Tab.Content>
        </Tab.Container>
      </Container>
    </div>
  );
};

export default ProductTab;
