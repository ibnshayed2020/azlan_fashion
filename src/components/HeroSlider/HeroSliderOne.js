import Swiper from "react-id-swiper";
import Link from "next/link";
import { Container } from "react-bootstrap";

const HeroSliderOne = ({ sliderData }) => {
  const params = {
    loop: true,
    speed: 1000,
    spaceBetween: 200,
    autoplay: {
      delay: 2000,
      disableOnInteraction: false,
    },
    watchSlidesVisibility: true,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev"
    },
    renderPrevButton: () => (
      <button className="swiper-button-prev ht-swiper-button-nav"></button>
    ),
    renderNextButton: () => (
      <button className="swiper-button-next ht-swiper-button-nav"></button>
    )
  };
  return (
    <div className="hero-slider-one space-mb--r100">
      <Container>
        <div className="hero-slider-one__wrapper">
          <Swiper {...params}>
            {sliderData &&
              sliderData.map((single) => {
                return (
                  <div
                    className="hero-slider-one__slide swiper-slide"
                    key={single.id}
                  >
                    <div className="slider-image">
                      <img
                        src={process.env.PUBLIC_URL + single.image}
                        className="img-fluid"
                        alt=""
                      />
                    </div>
                    <div className="slider-content">
                      <h2 className="color-title color-title--blue space-mb--20">
                        {single.subtitle}
                      </h2>
                      <h1
                        className="main-title space-mb--30"
                        dangerouslySetInnerHTML={{ __html: single.title }}
                      />

                        <a href={single.url} target={"_blank"} className="lezada-button lezada-button--medium">
                          shop now
                        </a>
                      {/*<a href={single.url} target={"_blank"} className="btn btn-dark btn-lg">*/}
                      {/*    SHOP NOW*/}
                      {/*  </a>*/}
                    </div>
                  </div>
                );
              })}
          </Swiper>
        </div>
      </Container>
    </div>
  );
};

export default HeroSliderOne;
