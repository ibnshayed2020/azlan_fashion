import {useEffect, useState} from "react";
import {Col, Container, Row} from "react-bootstrap";
import {FaFacebookF, FaInstagram, FaYoutube} from "react-icons/fa";
import {IoIosArrowRoundUp} from "react-icons/io";
import {animateScroll} from "react-scroll";

const FooterTwo = () => {
  const [scroll, setScroll] = useState(0);
  const [top, setTop] = useState(0);

  useEffect(() => {
    setTop(100);
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const scrollToTop = () => {
    animateScroll.scrollToTop();
  };

  const handleScroll = () => {
    setScroll(window.scrollY);
  };
  return (
    <footer className="bg-color--grey space-pt--100 space-pb--50">
      <Container className="wide">
        <Row>
          <Col className="footer-single-widget space-mb--50">
            {/* logo */}
            <div className="logo space-mb--35">
              {/*<img*/}
              {/*  src={process.env.PUBLIC_URL + "/assets/images/logo.png"}*/}
              {/*  className="img-fluid"*/}
              {/*  alt=""*/}
              {/*/>*/}
              <h1 className={"font-italic"} style={{letterSpacing:"8px",fontFamily: "serif",opacity:"0.8"}}><span style={{fontSize:"58px"}}>ʌ</span>ZL<span style={{fontSize:"58px"}}>ʌ</span>N</h1>
            </div>

            {/*=======  copyright text  =======*/}
            <div className="footer-single-widget__copyright">
              &copy; {new Date().getFullYear() + " "}
              <a href="https://www.hasthemes.com" target="_blank">
                AZLAN
              </a>
              <span>All Rights Reserved</span>
            </div>
          </Col>

          <Col className="footer-single-widget space-mb--50">
            <h5 className="footer-single-widget__title">ABOUT</h5>
            <nav className="footer-single-widget__nav">
              <ul>
                {/*<li>*/}
                {/*  <a href="/other/contact/">About us</a>*/}
                {/*</li>*/}
                {/*<li>*/}
                {/*  <a href="#">Store location</a>*/}
                {/*</li>*/}
                <li>
                  <a href="/other/contact">Contact Us</a>
                </li>
                {/*<li>*/}
                {/*  <a href="#">Orders tracking</a>*/}
                {/*</li>*/}
              </ul>
            </nav>
          </Col>

          {/*<Col className="footer-single-widget space-mb--50">*/}
          {/*  <h5 className="footer-single-widget__title">USEFUL LINKS</h5>*/}
          {/*  <nav className="footer-single-widget__nav">*/}
          {/*    <ul>*/}
          {/*      <li>*/}
          {/*        <a href="#">Returns</a>*/}
          {/*      </li>*/}
          {/*      <li>*/}
          {/*        <a href="#">Support Policy</a>*/}
          {/*      </li>*/}
          {/*      <li>*/}
          {/*        <a href="#">Size guide</a>*/}
          {/*      </li>*/}
          {/*      <li>*/}
          {/*        <a href="#">FAQs</a>*/}
          {/*      </li>*/}
          {/*    </ul>*/}
          {/*  </nav>*/}
          {/*</Col>*/}

          <Col className="footer-single-widget space-mb--50">
            <h5 className="footer-single-widget__title">FOLLOW US ON</h5>
            <nav className="footer-single-widget__nav footer-single-widget__nav--social">
              <ul>
                {/*<li>*/}
                {/*  <a href="https://www.twitter.com">*/}
                {/*    <FaTwitter /> Twitter*/}
                {/*  </a>*/}
                {/*</li>*/}
                <li>
                  <a target={"_blank"} href="https://www.facebook.com/floverrun/">
                    <FaFacebookF /> Facebook
                  </a>
                </li>
                <li>
                  <a target={"_blank"} href="https://www.instagram.com/azlan_street_wear/">
                    <FaInstagram /> Instagram
                  </a>
                </li>
                <li>
                  <a target={"_blank"} href="https://www.youtube.com/channel/UCzn51s2BcyqOd43FnXIBrNg">
                    <FaYoutube /> Youtube
                  </a>
                </li>
              </ul>
            </nav>
          </Col>

          {/*<Col className="footer-single-widget space-mb--50">*/}
          {/*  <div className="footer-subscribe-widget">*/}
          {/*    <h2 className="footer-subscribe-widget__title">Subscribe.</h2>*/}
          {/*    <p className="footer-subscribe-widget__subtitle">*/}
          {/*      Subscribe to our newsletter to receive news on update.*/}
          {/*    </p>*/}
          {/*    /!* email subscription *!/*/}
          {/*    <SubscribeEmailTwo mailchimpUrl="https://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef" />*/}
          {/*  </div>*/}
          {/*</Col>*/}
        </Row>
      </Container>
      <button
        className={`scroll-top ${scroll > top ? "show" : ""}`}
        onClick={() => scrollToTop()}
      >
        <IoIosArrowRoundUp />
      </button>
    </footer>
  );
};

export default FooterTwo;
